<?php 
   $output = '<div'. $attributes .'>';
   $output .=  '<div' .$content_attributes .'>';
   if ($linked_logo_img || $site_name || $site_slogan){ 
     $output .= '<div class="branding-data">';
     if ($linked_logo_img){
        $output .= '<div class="logo-img">'. $linked_logo_img .'</div>';


     }
     if ($site_name || $site_slogan){ 
       $class = $site_name_hidden && $site_slogan_hidden ? ' element-invisible' : ''; 
       $output .='<hgroup class="site-name-slogan' . $class.'">';        
       if ($site_name){
          $class = $site_name_hidden ? ' element-invisible' : ''; 
          if ($is_front){
            $output.= '<h1 class="site-name' . $class. '">' . $linked_site_name . '</h1>';
          }else {
            $output .= '<h2 class="site-name' .$class. '">' . $linked_site_name . '</h2>';
          }
       }
       if ($site_slogan){
         $class = $site_slogan_hidden ? ' element-invisible' : ''; 
         $output .= '<h6 class="site-slogan' . $class . '">' . $site_slogan . '</h6>';
       }
       $output .= '</hgroup>';
     }
     $output .= '</div>';
   }
   $output .= $content . ' </div> </div>';

print $output;

