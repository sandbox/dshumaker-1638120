<?php 

$tag = $block->subject ? 'section' : 'div'; 

$output = '<'.$tag . $attributes .'>';
// Take out clearfix if in the navigation
$nofix = false;
$nofix = strpos($attributes, 'block-superfish') || strpos($attributes, 'block-block-1');
if ($nofix){
  $output .= '<div class="block-inner">';
}else {
  $output .= '<div class="block-inner clearfix">';
} 
 $output .= render($title_prefix); 
 if ($block->subject){ 
   $output .= '<h2' . $title_attributes . $block->subject .'</h2>';
 }
 $output .= render($title_suffix); 

 // take out the clearfix
 if ($nofix) {
   $content_attributes = str_replace('clearfix', '', $content_attributes);
 }
 $output .= '<div' . $content_attributes . '>' . $content . '</div></div></' . $tag . '>';

 print $output;
