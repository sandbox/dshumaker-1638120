/**
 * animateBj
 * jQuery plugin by Luc Martin
 * 
 */

(function($) {
	
	$.fn.animateBg = function(args) {
		
		//consolidate the 'this' for the root object
		var base = this;
		
		//Iterate trough all elements set by the plugin
		return this.each(function() {
			
			if( typeof (args.backgroundPositionTop ) === "undefined" || args.backgroundPositionTop === null)
				args.backgroundPositionTop = 0;
				
			if( typeof (args.backgroundPositionLeft ) === "undefined" || args.backgroundPositionLeft === null)
				args.backgroundPositionLeft = 0;
				
			if( typeof (args.moveSpeed ) === "undefined" || args.moveSpeed === null)
				args.moveSpeed = 100;
				
			if( typeof (args.moveDistance ) === "undefined" || args.moveDistance === null)
				args.moveDistance = 1;
			
			if( typeof (args.direction ) === "undefined" || args.direction === null)
				args.direction = 'h';
				
			animateTheBg(args);
			
		});

		function animateTheBg(args){
			
			// speed in milliseconds
			var scrollSpeed = args.moveSpeed;
			var direction = args.direction;
			
			// set the default position
			var current = 0;
			
			function bgscroll(){
				
				// 1 pixel row at a time
				current += args.moveDistance;
				
				// move the background with backgrond-position css properties
				//$(base).css("backgroundPosition", (direction == 'h') ? current+"px "+args.backgroundPositionTop+"px" : args.backgroundPositionLeft+"px" + current+"px");
				$(base).css("backgroundPosition", args.backgroundPositionTop+"px "+current+"px");
				
			}
			//Calls the scrolling function repeatedly
			var init = setInterval(function(){ 
				bgscroll(); 
			}, scrollSpeed);
		}
	};
})(jQuery);

$(document).ready(function(){
	$('#zone-branding').animateBg({   
    backgroundPositionTop:0, 
    backgroundPositionLeft:0, 
    moveSpeed: 10, 
    moveDistance:1, 
    direction: 'h' });
});
